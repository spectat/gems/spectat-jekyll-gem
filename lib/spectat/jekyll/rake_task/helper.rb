require 'erubis'
require 'spectat/fqdn'

module Spectat
  module Jekyll
    module RakeTask
      # Helper class for standardising Docker configuration
      class DockerHelper
        attr_reader :full_repo_path
        attr_reader :image_type
        attr_reader :image_tag

        VALID_IMAGE_TYPES = %i[builder built deploy].freeze

        def initialize(full_repo_path, image_type, image_tag)
          raise ArgumentError, 'image_type must be ' + VALID_IMAGE_TYPES.to_s unless VALID_IMAGE_TYPES.include?(image_type)

          @full_repo_path = full_repo_path
          @image_type = image_type
          @image_tag = image_tag
          @image_name = case @image_type
                        when :built
                          ''
                        else
                          "/#{@image_type}"
                        end
        end

        def image_path
          "registry.gitlab.com/#{@full_repo_path}#{@image_name}:#{@image_tag}"
        end

        def docker_build_command
          "docker build --pull --cache-from #{image_path} --tag #{image_path} --file #{path_to_dockerfile(__FILE__)} ."
        end

        def path_to_dockerfile(executing_script_path)
          dockerfile = "Dockerfile.#{@image_type}.erb"
          full_path = File.join(File.dirname(executing_script_path), dockerfile)
          raise IOError, "#{full_path} doesn't exist" unless File.file?(full_path)
          full_path
        end
      end

      # Helper class for generating standardised GitLab CI configuration
      class GitLabHelper
        attr_reader :fqdn
        attr_reader :full_repo_path

        def initialize(full_repo_path, fqdn)
          @full_repo_path = full_repo_path
          @fqdn = fqdn
        end

        def render_template
          template = File.read(File.join(File.dirname(__FILE__), 'gitlab-ci.yml.erb'))
          erb = Erubis::Eruby.new(template)

          staging_helper = Spectat::Fqdn.new(@fqdn, 'staging')
          review_helper = Spectat::Fqdn.new(staging_helper.review_apps_fqdn, 'staging')

          erb.evaluate(
            fqdn_production: Spectat::Fqdn.new(@fqdn, 'production').fqdn,
            fqdn_staging: staging_helper.fqdn,
            fqdn_suffix_review: review_helper.fqdn,
            full_repo_path: @full_repo_path
          )
        end
      end

      # Helper class for generating standardised Capistrano configuration
      class CapistranoHelper
        attr_reader :fqdn
        attr_reader :full_repo_path

        def initialize(full_repo_path, fqdn)
          @full_repo_path = full_repo_path
          @fqdn = fqdn
        end

        def render_capfile
          File.read(File.join(File.dirname(__FILE__), 'Capfile.erb'))
        end

        def render_deployrb
          template = File.read(File.join(File.dirname(__FILE__), 'config-deploy.rb.erb'))
          erb = Erubis::Eruby.new(template)

          erb.evaluate(
            fqdn: @fqdn,
            full_repo_path: @full_repo_path
          )
        end
      end
    end
  end
end

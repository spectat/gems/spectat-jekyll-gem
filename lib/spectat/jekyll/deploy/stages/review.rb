require 'spectat/fqdn'

fqdn_str = Spectat::Fqdn.review_apps_fqdn(fetch(:application))
fqdn_helper = Spectat::Fqdn.new(fqdn_str, 'staging')

set :deploy_to, "#{fqdn_helper.site_dir}/#{fetch :tag}"

server fqdn_helper.fqdn, user: fqdn_helper.user, roles: %(app)

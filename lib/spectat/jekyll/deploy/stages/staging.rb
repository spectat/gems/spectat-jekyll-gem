require 'spectat/fqdn'

fqdn_helper = Spectat::Fqdn.new(fetch(:application), fetch(:stage))
set :deploy_to, fqdn_helper.site_dir

server fqdn_helper.fqdn, user: fqdn_helper.user, roles: %(app)

require 'spectat/jekyll/rake_task/helper'

namespace :docker do
  desc 'Build site\'s image'
  task :build, [:full_repo_path, :image_type, :image_tag] do |_, args|
    helper = Spectat::Jekyll::RakeTask::DockerHelper.new(args[:full_repo_path], args[:image_type].to_sym, args[:image_tag])
    sh helper.docker_build_command
  end
end

namespace :gitlab do
  desc 'Generate the .gitlab-ci.yml file to a given `filename`'
  task :generate_path, [:filename, :full_repo_path, :fqdn] do |_, args|
    helper = Spectat::Jekyll::RakeTask::GitLabHelper.new(args[:full_repo_path], args[:fqdn])
    File.write(args[:filename], helper.render_template)
  end

  desc 'Generate the .gitlab-ci.yml file'
  task :generate, [:full_repo_path, :fqdn] do |_, args|
    Rake::Task['gitlab:generate_path'].invoke('.gitlab-ci.yml', args[:full_repo_path], args[:fqdn])
  end

  desc 'Validate whether .gitlab-ci.yml has been updated'
  task :validate, [:full_repo_path, :fqdn] do |_, args|
    Rake::Task['gitlab:generate_path'].invoke('.gitlab-ci.new.yml', args[:full_repo_path], args[:fqdn])
    sh 'diff -au .gitlab-ci.yml .gitlab-ci.new.yml'
  end
end

namespace :capistrano do # rubocop:disable Metrics/BlockLength
  namespace :capfile do
    desc 'Generate the Capfile file to a given `filename`'
    task :generate_path, [:filename, :full_repo_path, :fqdn] do |_, args|
      helper = Spectat::Jekyll::RakeTask::CapistranoHelper.new(args[:full_repo_path], args[:fqdn])
      File.write(args[:filename], helper.render_capfile)
    end

    desc 'Generate the Capfile file'
    task :generate, [:full_repo_path, :fqdn] do |_, args|
      Rake::Task['capistrano:capfile:generate_path'].invoke('Capfile', args[:full_repo_path], args[:fqdn])
    end

    desc 'Validate whether Capfile has been updated'
    task :validate, [:full_repo_path, :fqdn] do |_, args|
      Rake::Task['capistrano:capfile:generate_path'].invoke('Capfile.new', args[:full_repo_path], args[:fqdn])
      sh 'diff -au Capfile Capfile.new'
    end
  end

  namespace :deployrb do
    desc 'Generate the config/deploy.rb file to a given `filename`'
    task :generate_path, [:filename, :full_repo_path, :fqdn] do |_, args|
      FileUtils.mkdir_p(File.dirname(args[:filename]))
      helper = Spectat::Jekyll::RakeTask::CapistranoHelper.new(args[:full_repo_path], args[:fqdn])
      File.write(args[:filename], helper.render_deployrb)
    end

    desc 'Generate the config/deploy.rb file'
    task :generate, [:full_repo_path, :fqdn] do |_, args|
      Rake::Task['capistrano:deployrb:generate_path'].invoke('config/deploy.rb', args[:full_repo_path], args[:fqdn])
    end

    desc 'Validate whether config/deploy.rb has been updated'
    task :validate, [:full_repo_path, :fqdn] do |_, args|
      Rake::Task['capistrano:deployrb:generate_path'].invoke('config/deploy.rb.new', args[:full_repo_path], args[:fqdn])
      sh 'diff -au config/deploy.rb config/deploy.rb.new'
    end
  end

  desc 'Generate Capfile and config/deploy.rb files'
  task :generate, [:full_repo_path, :fqdn] do |_, args|
    Rake::Task['capistrano:capfile:generate'].invoke(args[:full_repo_path], args[:fqdn])
    Rake::Task['capistrano:deployrb:generate'].invoke(args[:full_repo_path], args[:fqdn])
  end

  desc 'Validate Capfile and config/deploy.rb are up-to-date'
  task :validate, [:full_repo_path, :fqdn] do |_, args|
    Rake::Task['capistrano:capfile:validate'].invoke(args[:full_repo_path], args[:fqdn])
    Rake::Task['capistrano:deployrb:validate'].invoke(args[:full_repo_path], args[:fqdn])
  end
end

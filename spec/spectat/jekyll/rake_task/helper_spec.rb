require 'spec_helper'
require 'spectat/jekyll/rake_task/helper'

RSpec.describe 'DockerHelper' do
  context '.initialize' do
    it 'as a :builder type' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :builder,
                                                        'latest')
      expect(sut.full_repo_path).to eq 'test/group/subgroup'
      expect(sut.image_type).to eq :builder
      expect(sut.image_tag).to eq 'latest'
    end

    it 'as a :built type' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :built,
                                                        'latest')
      expect(sut.full_repo_path).to eq 'test/group/subgroup'
      expect(sut.image_type).to eq :built
      expect(sut.image_tag).to eq 'latest'
    end

    it 'as a :deploy type' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :deploy,
                                                        'latest')
      expect(sut.full_repo_path).to eq 'test/group/subgroup'
      expect(sut.image_type).to eq :deploy
      expect(sut.image_tag).to eq 'latest'
    end

    it 'throws an error when not a valid type' do
      expect { Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup', :wibble, 'latest') }.to \
        raise_error(ArgumentError, 'image_type must be [:builder, :built, :deploy]')
    end
  end

  context '.image_path' do
    it ':builder has ***' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :builder,
                                                        'latest')
      expect(sut.image_path).to eq 'registry.gitlab.com/test/group/subgroup/builder:latest'
    end

    it ':built has no ***' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :built,
                                                        'latest')
      expect(sut.image_path).to eq 'registry.gitlab.com/test/group/subgroup:latest'
    end

    it ':deploy has ***' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :deploy,
                                                        'latest')
      expect(sut.image_path).to eq 'registry.gitlab.com/test/group/subgroup/deploy:latest'
    end
  end

  context '.docker_build_command' do
    it 'has the image_path and path_to_dockerfile for :builder' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :builder,
                                                        'latest')
      expect(sut).to receive(:path_to_dockerfile).and_return '/path/to/Dockerfile.builder'
      expect(sut.docker_build_command).to eq \
        'docker build --pull --cache-from registry.gitlab.com/test/group/subgroup/builder:latest ' \
        '--tag registry.gitlab.com/test/group/subgroup/builder:latest --file /path/to/Dockerfile.builder .'
    end

    it 'has the image_path and path_to_dockerfile for :built' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :built,
                                                        'latest')
      expect(sut).to receive(:path_to_dockerfile).and_return '/some/path/to/Dockerfile.built'
      expect(sut.docker_build_command).to eq \
        'docker build --pull --cache-from registry.gitlab.com/test/group/subgroup:latest --tag ' \
        'registry.gitlab.com/test/group/subgroup:latest --file /some/path/to/Dockerfile.built .'
    end

    it 'has the image_path and path_to_dockerfile for :deploy' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :deploy,
                                                        'latest')
      expect(sut).to receive(:path_to_dockerfile).and_return '/another/path/to/Dockerfile.deploy'
      expect(sut.docker_build_command).to eq \
        'docker build --pull --cache-from registry.gitlab.com/test/group/subgroup/deploy:latest --tag ' \
        'registry.gitlab.com/test/group/subgroup/deploy:latest --file /another/path/to/Dockerfile.deploy .'
    end
  end

  context '.path_to_dockerfile' do
    it 'returns if path exists' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :builder,
                                                        'latest')
      expect(File).to receive(:file?).with('/some/path/to/Dockerfile.builder.erb').and_return true
      expect(sut.path_to_dockerfile('/some/path/to/script.rb')).to eq '/some/path/to/Dockerfile.builder.erb'
    end

    it 'raises if it doesn\'t exist' do
      sut = Spectat::Jekyll::RakeTask::DockerHelper.new('test/group/subgroup',
                                                        :deploy,
                                                        'latest')
      expect(File).to receive(:file?).with('/pretend/path/to/Dockerfile.deploy.erb').and_return false
      expect { sut.path_to_dockerfile('/pretend/path/to/script.rb') }.to \
        raise_error(IOError, '/pretend/path/to/Dockerfile.deploy.erb doesn\'t exist')
    end
  end
end

RSpec.describe 'GitLabHelper' do
  context '.initialize' do
    it 'takes a full_repo_path and an FQDN' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      expect(sut.full_repo_path).to eq 'spectat/client/site.co.uk'
      expect(sut.fqdn).to eq 'www.client-site.co.uk'
    end
  end

  context '.render_gitlab_ci_yml' do
    it 'renders the FQDNs for GitLab Environments' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_template
      expect(template).to match %r{URL_PRODUCTION: https://www.client-site.co.uk}
      expect(template).to match %r{URL_STAGING: https://www.staging.client-site.co.uk}
      expect(template).to match %r{URL_REVIEW_SUFFIX: www.review.client-site.co.uk}
    end

    it 'renders the CONTAINER_IMAGE_BASE_URL' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_template
      expect(template).to match %r{CONTAINER_IMAGE_BASE_URL: registry.gitlab.com/spectat/client/site.co.uk}
    end

    it 'renders the SSH keys' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_template
      expect(template).to match %r{- echo -e "\$REVIEW_SSH_PRIVATE_KEY" \| ssh-add -$}
      expect(template).to match %r{- echo -e "\$STAGING_SSH_PRIVATE_KEY" \| ssh-add -$}
      expect(template).to match %r{- echo -e "\$PRODUCTION_SSH_PRIVATE_KEY" \| ssh-add -$}
    end

    it 'uses the Rake docker tasks' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_template
      expect(template).to match %r{- bundle exec rake "docker:build\[spectat/client/site\.co\.uk,builder,\$CI_COMMIT_REF_SLUG\]"}
      expect(template).to match %r{- bundle exec rake "docker:build\[spectat/client/site\.co\.uk,built,\$CI_COMMIT_REF_SLUG\]"}
      expect(template).to match %r{- bundle exec rake "docker:build\[spectat/client/site\.co\.uk,deploy,\$CI_COMMIT_REF_SLUG\]"}
    end

    it 'renders the _IMAGE paths for each type of image (i.e. `/deploy` and ``)' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_template
      expect(template).to match %r{CONTAINER_BUILT_IMAGE: \$CONTAINER_IMAGE_BASE_URL:\$CI_COMMIT_REF_SLUG$}
      expect(template).to match %r{CONTAINER_BUILDER_IMAGE: \$CONTAINER_IMAGE_BASE_URL/builder:\$CI_COMMIT_REF_SLUG$}
      expect(template).to match %r{CONTAINER_DEPLOY_IMAGE: \$CONTAINER_IMAGE_BASE_URL/deploy:\$CI_COMMIT_REF_SLUG$}
    end

    it 'validates the .gitlab-ci.yml file ' do
      sut = Spectat::Jekyll::RakeTask::GitLabHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_template
      expect(template).to match %r{- bundle exec rake 'gitlab:validate\[spectat/client/site.co.uk,www.client-site.co.uk\]'}
    end
  end
end

RSpec.describe 'CapistranoHelper' do
  context '.initialize' do
    it 'takes a full_repo_path and an FQDN' do
      sut = Spectat::Jekyll::RakeTask::CapistranoHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      expect(sut.full_repo_path).to eq 'spectat/client/site.co.uk'
      expect(sut.fqdn).to eq 'www.client-site.co.uk'
    end
  end

  context '.render_capfile' do
    it 'renders Capfile' do
      sut = Spectat::Jekyll::RakeTask::CapistranoHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_capfile
      expect(template).to match %r{require 'spectat/jekyll/deploy'}
    end
  end

  context '.render_deployrb' do
    it 'renders config/deploy.rb' do
      sut = Spectat::Jekyll::RakeTask::CapistranoHelper.new('spectat/client/site.co.uk', 'www.client-site.co.uk')
      template = sut.render_deployrb
      expect(template).to match %r{set :application, 'www.client-site.co.uk'}
      expect(template).to match %r{set :full_repo_path, 'spectat/client/site.co.uk'}
    end
  end
end
